var TYPE_NUMERIC = 1;
var TYPE_VISUAL = 2;
var TYPE_WORDED = 4;

var NUMBER_WORDS = {
	1: 'one', 2: 'two', 3: 'three', 4: 'four', 5: 'five', 6: 'six', 7: 'seven', 8: 'eight', 9: 'nine',

	10: 'ten', 11: 'eleven', 12: 'twelve', 13: 'thirteen', 14: 'fourteen', 15: 'fifteen', 16: 'sixteen', 17: 'seventeen', 18: 'eighteen', 19: 'nineteen',

	20: 'twenty', 30: 'thirty', 40: 'forty', 50: 'fifty', 60: 'sixty', 70: 'seventy', 80: 'eighty', 90: 'ninety',

	100: 'hundred',
	1000: 'thousand'
};

var DENOMINATOR_WORDS = {
	'-1': 'firsts',
	'-2': 'seconds',

	1: 'wholes', 2: 'halves', 3: 'thirds', 4: 'fourths', 5: 'fifths', 6: 'sixths', 7: 'sevenths', 8: 'eights', 9: 'nineths',

	10: 'tenths', 11: 'elevenths', 12: 'twelths', 13: 'thirteenths', 14: 'fourteenths', 15: 'fifteenths', 16: 'sixteenths', 17: 'seventeenths', 18: 'eighteenths', 19: 'nineteeths',

	20: 'twentieths', 30: 'thirtieths', 40: 'fortieths', 50: 'fiftieths', 60: 'sixtieths', 70: 'seventieths', 80: 'eightieths', 90: 'ninetieths',

	100: 'hundredths',
	1000: 'thousandths'
};

function Fraction(string) {
	this.originalValue = string;
}

Fraction.prototype = {
	originalValue: undefined,
	isVerified: undefined,

	type: TYPE_VISUAL,
	wholeNumber: 0,
	numerator: 0,
	denominator: 0,

	verify: function () {
		if (this.isVerified !== undefined) {
			return this.isVerified;
		}

		// Attempt verification
		var regex = /(?:([^\s\d]+)\s*)?(?:(\d+)\s+)?(\d+)\s*\/\s*(\d+)/i;
		var matches = this.originalValue.match(regex);

		if (matches === null) {
			this.isVerified = false;
			return false;
		}

		// Convert type to internal value
		var type = String(matches[1] || 'v').toLowerCase();

		switch (type) {
		case 'n': type = TYPE_NUMERIC; break;
		case 'v': type = TYPE_VISUAL; break;
		case 'w': type = TYPE_WORDED; break;
		default:
			this.isVerified = false;
			return false;
		}

		// Parse out other values
		var wholeNumber = ~~matches[2];
		var numerator = ~~matches[3];
		var denominator = ~~matches[4];

		//
		this.type = type;
		this.wholeNumber = wholeNumber;
		this.numerator = numerator;
		this.denominator = denominator;

		//
		this.isVerified = true;
		return true;
	},

	convert: function (toType) {
		if (!this.verify()) {
			return this.originalString;
		}

		toType = toType || this.type;

		switch (toType) {
		case TYPE_NUMERIC:
			return this.convertToNumeric();
		case TYPE_VISUAL:
			return this.convertToVisual();
		case TYPE_WORDED:
			return this.convertToWorded();
		}
	},

	convertToNumeric: function () {
		return (this.wholeNumber + (this.numerator / this.denominator)).toFixed(2);
	},

	convertToVisual: function () {
		var value = '<sup>' + this.numerator + '</sup>⁄<sub>' + this.denominator + '</sub>';

		if (this.wholeNumber) {
			value = this.wholeNumber + ' ' + value;
		}

		return value;
	},

	convertToWorded: function () {
		var value = '';

		if (this.wholeNumber) {
			value = this.convertTermToWord(this.wholeNumber);
			value += ' and ';
		}

		// Use "numerator over denominator" form
		if (this.denominator > 10) {
			value += this.convertTermToWord(this.numerator);
			value += ' over ';
			value += this.convertTermToWord(this.denominator);

		// Use "numerator denominator-ths" form
		} else {
			value += this.convertTermToWord(this.numerator);
			value += ' ';
			value += this.convertTermToWord(this.denominator, true);
		}

		// Remove extra spaces
		value = value
		.replace(/\s+/g, ' ')
		.replace(/^\s*|\s*$/g, '');

		return value;
	},

	convertTermToWord: function (term, useDenominatorSuffix) {
		if (!term) { return ''; }
		if (term.length > 4) { return 'N/A'; }

		// Split term into single-digits
		var terms = String(term).split('');
		var previousPayload;

		// Process each digit
		terms = terms
		.reverse()
		.map(function convertDigitToWord(digit, place) {
			digit = ~~digit;

			var payload = {
				word: false,
				digit: digit,
				place: place
			};

			if (!digit) {
				previousPayload = payload;
				return payload;
			}

			// Convert digit to proper word based on place
			var places = Math.pow(10, place);

			// Is tens
			if (place === 1) {
				digit = digit * 10;

				// If digit < 20, use teens names
				if (digit < 20) {
					// Disable previous payload so it'll get removed later
					previousPayload.word = false;

					// Combine payloads for teens name
					payload.digit = digit + previousPayload.digit;
					payload.word = NUMBER_WORDS[payload.digit];

				// Otherwise use tens names
				} else {
					payload.word = NUMBER_WORDS[digit];
				}

			// Is largers than tens
			} else if (place > 1) {
				payload.word = NUMBER_WORDS[digit] + ' ' + NUMBER_WORDS[places];

			// Is ones
			} else {
				payload.word = NUMBER_WORDS[digit];
			}

			//
			previousPayload = payload;
			return payload;
		})
		.reverse()
		.filter(function filterOutEmpties(term) {
			return term.word;
		});

		// Do we add suffix?
		if (useDenominatorSuffix) {
			var lastTerm = terms.pop();
			var nextToLastTerm = terms[terms.length - 1] || {};

			var digit = ~~lastTerm.digit;
			var place = lastTerm.place;
			var places = Math.pow(10, place);

			// Is tens
			if (place === 1) {
				lastTerm.word = DENOMINATOR_WORDS[digit];

			// Is higher than tens
			} else if (place > 1) {
				lastTerm.word = DENOMINATOR_WORDS[digit] + ' ' + DENOMINATOR_WORDS[places];

			// Is ones
			} else {
				// If last digit is 2 or less and there's a higher
				// place value, use firsts and seconds instead of
				// wholes and halves
				if (digit < 3 && nextToLastTerm.place > 0) {
					digit *= -1;
				}

				lastTerm.word = DENOMINATOR_WORDS[digit];
			}

			terms.push(lastTerm);
		}

		//
		return terms.map(function mapToWord(payload) {
			return payload.word;
		}).join(' ');
	},

	label: function () {
		return Fraction.Label[this.type];
	}
};

Fraction.create = function (string) {
	var parser = new this(string);
	return parser;
};

Fraction.Type = {
	Numeric: TYPE_NUMERIC,
	Visual: TYPE_VISUAL,
	Worded: TYPE_WORDED
};

Fraction.Label = {
	1: 'numeric',
	2: 'visual',
	4: 'worded'
};

module.exports = Fraction;
