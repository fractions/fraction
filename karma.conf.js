var webpackConfig = require('./webpack.conf.js');

module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    files: [
		'test/**/*.test.es6'
    ],
    preprocessors: {
		'test/**/*.test.es6': ['webpack']
    },
	webpack: webpackConfig.development(),
	webpackMiddleware: {
		noInfo: true
	},
	plugins: [
		require('karma-webpack'),
		require('karma-jasmine'),
		require('karma-chrome-launcher')
	],
    reporters: ['dots'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: false
  });
};
